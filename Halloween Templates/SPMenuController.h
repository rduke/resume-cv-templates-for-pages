//
//  SPMenuController.h
//  Resume & CV Templates for Pages
//
//  Created by  Farid Dahiri on 1/29/16.
//  Copyright © 2016 PixelBox. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AppKit/NSMenuItem.h>

@interface SPMenuController : NSObject

- (IBAction)onHelp:(id)sender;
- (IBAction)onContactByMail:(NSMenuItem *)sender;

@end
