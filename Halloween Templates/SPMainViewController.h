//
//  SPMainViewController.h
//  Iwork Templates
//
//  Created by PixelBox on 02/09/15.
//  Copyright (c) 2015 PixelBox. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Quartz/Quartz.h>

@class SPCustomImagesView;



@interface SPMainViewController : NSViewController{
    
    BOOL isConfigured;
    
    
    
}

@property (nonatomic, retain) IBOutlet SPCustomImagesView *imagesView;
@property (nonatomic, retain) IBOutlet NSScrollView *scrollView;


@property (nonatomic, retain) NSMutableArray *images;
@property (nonatomic, retain) NSMutableArray *importedImages;








+(SPMainViewController *)sharedMainController;


-(void)readItemsFromPath:(NSString *)path;
- (void)addImagesWithPaths:(NSArray *)urls;





@end
