//
//  AppDelegate.m
//  Halloween Templates
//
//  Created by PixelBox on 30/09/15.
//  Copyright (c) 2015 PixelBox. All rights reserved.
//

#import "AppDelegate.h"
#import "SPMainViewController.h"


@interface AppDelegate ()

@property (weak) IBOutlet NSView *imagesBrowserView;


@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
    
    self.mainViewController = [[SPMainViewController alloc] initWithNibName:@"SPMainViewController" bundle:nil];
    [self.window.contentView addSubview:self.mainViewController.view];
    
    
    
    //read all pages templates
    
    NSString *fullPath = [NSString stringWithFormat:@"%@/Templates/Pages Templates",[[NSBundle mainBundle] resourcePath]];
    
    
    [self.mainViewController readItemsFromPath:fullPath];
    
    
    
    
    
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)theApplication {
    return YES;
}

@end
