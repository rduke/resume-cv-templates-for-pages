//
//  SPTemplate.m
//  Iwork Templates
//
//  Created by PixelBox on 02/09/15.
//  Copyright (c) 2015 PixelBox. All rights reserved.
//

#import "SPTemplate.h"

@implementation SPTemplate

@synthesize path = _path;


+(SPTemplate *)emptyTemplate{
    
    SPTemplate *aTemplate = [[SPTemplate alloc] init];
    
    aTemplate.path = @"emptyPath";
    
    
    
    return aTemplate;
    
}


/* required methods of the IKImageBrowserItem protocol */
#pragma mark -
#pragma mark item data source protocol

/* let the image browser knows we use a path representation */
- (NSString *)imageRepresentationType
{
    return IKImageBrowserPathRepresentationType;
}

/* give our representation to the image browser */
- (id)imageRepresentation
{
    return _path;
}

/* use the absolute filepath as identifier */
- (NSString *)imageUID
{
    return _path;
}

// -------------------------------------------------------------------------
//	imageTitle:
//
//	Use the last path component as the title.
// -------------------------------------------------------------------------
- (NSString*)imageTitle
{
    return [[_path lastPathComponent] stringByDeletingPathExtension];
}

// -------------------------------------------------------------------------
//	imageSubtitle:
//
//	Use the file extension as the subtitle.
// -------------------------------------------------------------------------
- (NSString*)imageSubtitle
{
    return [_path pathExtension];
}

@end
