//
//  SPHelpWindowController.m
//  Resume & CV Templates for Pages
//
//  Created by  Farid Dahiri on 1/29/16.
//  Copyright © 2016 PixelBox. All rights reserved.
//

#import "SPHelpWindowController.h"

@interface SPHelpWindowController ()

@end

@implementation SPHelpWindowController

- (void)windowDidLoad {
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
    NSString* indexPath = [[NSBundle mainBundle] pathForResource:@"index" ofType:@"html"];
    [[NSFileManager defaultManager] contentsAtPath:indexPath];
    
    [[self.webView mainFrame] loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:indexPath]]];
}

@end
