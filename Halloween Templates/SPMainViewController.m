//
//  SPMainViewController.m
//  Iwork Templates
//
//  Created by PixelBox on 02/09/15.
//  Copyright (c) 2015 PixelBox. All rights reserved.
//

#define TempalteSuffix @" 800x600"

#import "SPMainViewController.h"
#import "AppDelegate.h"
#import "SPTemplate.h"
#import "SPCustomImagesView.h"



@interface SPMainViewController ()

@end

@implementation SPMainViewController


@synthesize images = _images;
@synthesize importedImages = _importedImages;
@synthesize imagesView = _imageBrowser;




- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do view setup here.
}

-(void)awakeFromNib{
    
    if (!isConfigured) {
        
        //alloc arrays
        self.images = [[NSMutableArray alloc] init];
        
        
        self.importedImages = [[NSMutableArray alloc] init];
        
        
        /*configure images view*/
        
        //allow reordering, animations et set draggind destination delegate
        [_imageBrowser setAllowsReordering:NO];
        [_imageBrowser setAnimates:YES];
        [_imageBrowser setDraggingDestinationDelegate:self];
        [_imageBrowser setAllowsMultipleSelection:NO];
        
        // customize the appearance
        [_imageBrowser setCellsStyleMask:IKCellsStyleTitled | IKCellsStyleOutlined];
        
        
        //set background
        
        //[_imageBrowser setValue:[NSColor colorWithDeviceRed:0.245f green:0.245f blue:0.245f alpha:1.0f] forKey:IKImageBrowserBackgroundColorKey];
        
        //NSColor *mainBackgroundColor = [NSColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:239.0/255.0 alpha:1.0];
        
        NSColor *mainBackgroundColor = [NSColor colorWithCalibratedRed:225.0/255.0 green:226.0/255.0 blue:229.0/255.0 alpha:1.0];
        
        
        
        [_imageBrowser setValue:mainBackgroundColor forKey:IKImageBrowserBackgroundColorKey];
        
        
        /*
         // background layer
         ImageBrowserBackgroundLayer *backgroundLayer = [[[ImageBrowserBackgroundLayer alloc] init] autorelease];
         [_imageBrowser setBackgroundLayer:backgroundLayer];
         backgroundLayer.owner = _imageBrowser;
         */
        
        //-- change default font
        // create a centered paragraph style
        NSMutableParagraphStyle *paraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paraphStyle setLineBreakMode:NSLineBreakByTruncatingTail];
        [paraphStyle setAlignment:NSCenterTextAlignment];
        
        NSMutableDictionary *attributes = [[NSMutableDictionary alloc] init] ;
        [attributes setObject:[NSFont systemFontOfSize:12] forKey:NSFontAttributeName];
        [attributes setObject:paraphStyle forKey:NSParagraphStyleAttributeName];
        [attributes setObject:[NSColor blackColor] forKey:NSForegroundColorAttributeName];
        [_imageBrowser setValue:attributes forKey:IKImageBrowserCellsTitleAttributesKey];
        
        attributes = [[NSMutableDictionary alloc] init];
        
        [attributes setObject:[NSFont boldSystemFontOfSize:12] forKey:NSFontAttributeName];
        [attributes setObject:paraphStyle forKey:NSParagraphStyleAttributeName];
        [attributes setObject:[NSColor whiteColor] forKey:NSForegroundColorAttributeName];
        
        [_imageBrowser setValue:attributes forKey:IKImageBrowserCellsHighlightedTitleAttributesKey];
        
        //change intercell spacin0
        [_imageBrowser setIntercellSpacing:NSMakeSize(20, 80)];
        
        //change selection color
        [_imageBrowser setValue:[NSColor colorWithCalibratedRed:1 green:0 blue:0.5 alpha:1.0] forKey:IKImageBrowserSelectionColorKey];
        
        //[_imageBrowser setContentResizingMask:NSViewHeightSizable];
        
        //[_imageBrowser setZoomValue:0.55];
        
        [_imageBrowser setCellSize:NSMakeSize(220.0, 220.0)];
        
        
        
        
        //config of view controller done
        isConfigured = TRUE;
        
        
    }
    
}




+(SPMainViewController *)sharedMainController{
    
    AppDelegate *delegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    
    return delegate.mainViewController;
    
    
}



/* entry point for reloading image-browser's data and setNeedsDisplay */
- (void)updateDatasource
{
    [_images removeAllObjects];
    
    //-- update our datasource, add recently imported items
    [_images addObjectsFromArray:_importedImages];
    
    //-- empty our temporary array
    [_importedImages removeAllObjects];
    
    //-- reload the image browser and set needs display
    [_imageBrowser reloadData];
    
    
}

#pragma mark -
#pragma mark import images from file system


-(void)readItemsFromPath:(NSString *)path{
    
    NSArray *temp = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:nil];
    
    NSArray *extensions = [NSArray arrayWithObjects:@"png", nil];
    
    NSArray *files = [temp filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"pathExtension IN %@", extensions]];
    
    NSMutableArray *fullPaths = [[NSMutableArray alloc] init];
    
    //make i > 1 to test multiple items
    
    for (int i = 0; i < 1; i++) {
        
        for (NSString *str in files) {
            
            NSString *fullPath = [NSString stringWithFormat:@"%@/%@",path,str];
            
            [fullPaths addObject:fullPath];
            
        }

        
        
    }
    
    if (fullPaths.count < 5) {
        
        
        NSInteger delta = 5-fullPaths.count;
        
        
        NSLog(@"Need to add %lu empty elements", delta);
        
        
        for (int i = 0; i < delta; i++) {
            
            [fullPaths addObject:@"emptyPath"];
            
            
        }
        
        
        
    }
    
    
     

    
    
    /*
    //NSLog(@"Reading items from path:%@",path);
    
    NSMutableArray *imagesURLs = [[NSMutableArray alloc] init];
    
    for (NSString *path in files) {
        
        NSURL *url = [NSURL fileURLWithPath:path];
        
        [imagesURLs addObject:url];
        
        
        
    }*/
    
    [self addImagesWithPaths:fullPaths];
    
    
    
    
    
}


/* Code that parse a repository and add all items in an independant array,
 When done, call updateDatasource, add these items to our datasource array
 This code is performed in an independant thread.
 */
- (void)addAnImageWithPath:(NSString *)path
{
    SPTemplate *p;
    
    NSString *fullPathToTemplate;
    
    NSString *templateName = [[path lastPathComponent] stringByDeletingPathExtension];
    
    NSString *pathBase = [path stringByDeletingLastPathComponent];
    
    
    
    
    //inspect path for app
    if ([path containsString:@"Pages Templates"]) {
        
        
        fullPathToTemplate = [NSString stringWithFormat:@"%@/%@.template",pathBase,templateName];
        
    }
    
    if ([path containsString:@"Numbers Templates"]) {
        
        fullPathToTemplate = [NSString stringWithFormat:@"%@/%@.nmbtemplate",pathBase,templateName];
        
    }
    
    if ([path containsString:@"Keynote Templates"]) {
        
        fullPathToTemplate = [NSString stringWithFormat:@"%@/%@.kth",pathBase,templateName];
        
    }
    
    if ([path isEqualToString:@"emptyPath"]) {
        
        p = [SPTemplate emptyTemplate];
        
        [_importedImages addObject:p];
        
        
        return;
        
        
    }

    
    
    /* add a path to our temporary array */
    p = [[SPTemplate alloc] init];
    
    [p setPath:path];
    
    p.pathToTemplate = fullPathToTemplate;
    
    
    
    
    [_importedImages addObject:p];
    
    //NSLog(@"%@", [[_imageBrowser cellForItemAtIndex:0] imageSubtitle]);
    
}

- (void)addImagesWithPath:(NSString *)path recursive:(BOOL)recursive
{
    NSInteger i, n;
    BOOL dir;
    
    [[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:&dir];
    
    if (dir)
    {
        NSArray *content = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:nil];
        
        n = [content count];
        
        // parse the directory content
        for (i=0; i<n; i++)
        {
            if (recursive)
                [self addImagesWithPath:[path stringByAppendingPathComponent:[content objectAtIndex:i]] recursive:YES];
            else
                [self addAnImageWithPath:[path stringByAppendingPathComponent:[content objectAtIndex:i]]];
        }
    }
    else
    {
        [self addAnImageWithPath:path];
    }
    
    
    
    if ([path isEqualToString:@"emptyPath"]) {
        [self addAnImageWithPath:path];
    }
}

/* performed in an independent thread, parse all paths in "paths" and add these paths in our temporary array */
- (void)addImagesWithPaths:(NSArray *)urls
{
    NSInteger i, n;
    
    
    n = [urls count];
    for ( i= 0; i < n; i++)
    {
       // NSURL *url = [urls objectAtIndex:i];
        [self addImagesWithPath:[urls objectAtIndex:i] recursive:NO];
    }
    
    /* update the datasource in the main thread */
    [self performSelectorOnMainThread:@selector(updateDatasource) withObject:nil waitUntilDone:YES];

}



#pragma mark -
#pragma mark IKImageBrowserDataSource

/* implement image-browser's datasource protocol
 Our datasource representation is a simple mutable array
 */

- (NSUInteger)numberOfItemsInImageBrowser:(IKImageBrowserView *)view
{
    /* item count to display is our datasource item count */
    return [_images count];
}

- (id)imageBrowser:(IKImageBrowserView *)view itemAtIndex:(NSUInteger)index
{
    return [_images objectAtIndex:index];
}


/* implement some optional methods of the image-browser's datasource protocol to be able to remove and reoder items */

/*	remove
	The user wants to delete images, so remove these entries from our datasource.
 */
- (void)imageBrowser:(IKImageBrowserView *)view removeItemsAtIndexes:(NSIndexSet *)indexes
{
    [_images removeObjectsAtIndexes:indexes];
}


//RIC double Click on Item
- (void) imageBrowser:(IKImageBrowserView *)view cellWasDoubleClickedAtIndex:(NSUInteger)index
{
    //[NSThread detachNewThreadSelector:@selector(openSelectedTheme:) toTarget:self withObject:nil];
    
    SPTemplate *template = [self.images objectAtIndex:index];
    
    [[NSWorkspace sharedWorkspace] openFile:template.pathToTemplate];
    
}


// reordering:
// The user wants to reorder images, update our datasource and the browser will reflect our changes
- (BOOL)imageBrowser:(IKImageBrowserView *)view moveItemsAtIndexes:(NSIndexSet *)indexes toIndex:(NSUInteger)destinationIndex
{
    NSUInteger index;
    NSMutableArray *temporaryArray;
    
    temporaryArray = [[NSMutableArray alloc] init];
    
    /* first remove items from the datasource and keep them in a temporary array */
    for (index = [indexes lastIndex]; index != NSNotFound; index = [indexes indexLessThanIndex:index])
    {
        if (index < destinationIndex)
            destinationIndex --;
        
        id obj = [_images objectAtIndex:index];
        [temporaryArray addObject:obj];
        [_images removeObjectAtIndex:index];
    }
    
    /* then insert removed items at the good location */
    NSInteger n = [temporaryArray count];
    for (index=0; index < n; index++)
    {
        [_images insertObject:[temporaryArray objectAtIndex:index] atIndex:destinationIndex];
    }
    
    return YES;
}




@end
