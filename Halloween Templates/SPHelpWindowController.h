//
//  SPHelpWindowController.h
//  Resume & CV Templates for Pages
//
//  Created by  Farid Dahiri on 1/29/16.
//  Copyright © 2016 PixelBox. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <WebKit/WebKit.h>


@interface SPHelpWindowController : NSWindowController

@property (nonatomic, strong)   IBOutlet    WebView* webView;

@end
