//
//  SPCustomImageCell.m
//  Iwork Templates
//PixelBox on 22/09/15.
//  Copyright (c) 2015 PixelBox. All rights reserved.
//

#import "SPCustomImageCell.h"

//iamges position in image browser
#define LEFTPADDING 30.0
#define TOPPADDING 18.7
#define CELLWIDTH 220.0
#define CELLHEIGHT 170.0
#define INTERCELLHORIZONTALSPACE 20.0
#define INTERCELLVERTICALSPACE 40.0


#define TITLESPACE 20.0

//selection handling
#define IMAGESELECTIONBORDERWIDTH 6.0
#define TITLESELECTIONADDITIONALSIZE 2.0

#import "SPTemplate.h"


@implementation SPCustomImageCell


-(NSRect)frame{
    
    //normally, you don't have to implement frame method, but there is strange alignment bug when only 1 row of elements is presented
    
    SPTemplate *item = (SPTemplate *)self.representedItem;
    
    
    
    
    NSRect frame = [super frame];
    
    
    if ([item.path isEqualToString:@"emptyPath"]) {
        
        return NSMakeRect(0.0, 0.0, 0.0, 0.0);
    }
    
    
    
    
    return frame;
    
    
    
    
}



-(NSRect)selectionFrame{
    
    
    return self.frame;
    
}


-(CALayer *)layerForType:(NSString *)type{
    
    
    
    
    
    if ([type isEqualToString:IKImageBrowserCellSelectionLayer]) {
    
        
        
        //root layer for two selection sublayers
        
        CALayer *layer = [[CALayer alloc] init];
        
        
        
        //**** Create image thumbnail selection sublayer
        
        
        //get rect of image
        NSRect imageRect = self.imageFrame;
        
        NSRect rootRect = self.selectionFrame;
        
        NSRect imageRectInRootrectCoordSystem = NSMakeRect(imageRect.origin.x - rootRect.origin.x - IMAGESELECTIONBORDERWIDTH, imageRect.origin.y - rootRect.origin.y - IMAGESELECTIONBORDERWIDTH, imageRect.size.width + 2*IMAGESELECTIONBORDERWIDTH, imageRect.size.height + 2*IMAGESELECTIONBORDERWIDTH);
        
        
        CALayer *imageSelectionLayer = [[CALayer alloc] init];
        
        
        imageSelectionLayer.frame = imageRectInRootrectCoordSystem;
        
        
        imageSelectionLayer.backgroundColor = CGColorCreateGenericRGB(158.0/255.0, 156.0/255.0, 159.0/255.0, 1.0);
        
        imageSelectionLayer.cornerRadius = 5.0;
        
        [layer addSublayer:imageSelectionLayer];
        
        
        //**** End of thumbnail selection sublayer creation
        
        
        //**** Create a title selection sublayer
        
        
        NSRect titleFrame = self.titleFrame;
        
        NSRect titleFrameInLocalCoordSystem = NSMakeRect(titleFrame.origin.x - rootRect.origin.x - 2*TITLESELECTIONADDITIONALSIZE, titleFrame.origin.y - rootRect.origin.y - TITLESELECTIONADDITIONALSIZE, titleFrame.size.width + 4*TITLESELECTIONADDITIONALSIZE, titleFrame.size.height + 2*TITLESELECTIONADDITIONALSIZE);
        
        
        
        CALayer *titleSelectionLayer  = [[CALayer alloc] init];
        
        titleSelectionLayer.frame = titleFrameInLocalCoordSystem;
        
        
        
        
        titleSelectionLayer.backgroundColor = CGColorCreateGenericRGB(158.0/255.0, 156.0/255.0, 159.0/255.0, 1.0);
        
        titleSelectionLayer.cornerRadius = 5.0;
        
        
        [layer addSublayer:titleSelectionLayer];
        
        
        //layer.backgroundColor = CGColorCreateGenericRGB(255.0, 0, 0, 1.0);
        
        
        
        return layer;
        
        
    }
    
    
    
    return [super layerForType:type];
    
    
    
}


-(NSRect)titleFrame{
    
    NSRect parentRect = [super titleFrame];
    
    //NSRect cellImageRect = self.imageFrame;
    
    NSRect cellRect = self.frame;
    
    
    
    //aligh to the center
    parentRect.origin.x = cellRect.origin.x + cellRect.size.width/2 - parentRect.size.width/2;
    
    //make a speca on y axis
    
    parentRect.origin.y = cellRect.origin.y;
    
    
    return parentRect;
    
    
    
}



-(NSImageAlignment)imageAlignment{
    
    
    
   // return NSImageAlignTopLeft;
    
    return NSImageAlignBottom;
    
    
}


-(NSRect)imageContainerFrame{
    
    
    NSRect cellFrame = self.frame;
    NSRect titleRect = self.titleFrame;
    
    
    
    return NSMakeRect(cellFrame.origin.x, cellFrame.origin.y + titleRect.size.height + TITLESPACE, cellFrame.size.width, cellFrame.size.height - titleRect.size.height - 2*TITLESPACE + 5);
    
    
    
    
}






@end
