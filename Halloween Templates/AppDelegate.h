//
//  AppDelegate.h
//  Halloween Templates
//
//  Created by PixelBox on 30/09/15.
//  Copyright (c) 2015 PixelBox. All rights reserved.
//

#import <Cocoa/Cocoa.h>
@class SPMainViewController;


@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (weak) IBOutlet NSWindow *window;

@property (nonatomic, retain) SPMainViewController *mainViewController;



@end

