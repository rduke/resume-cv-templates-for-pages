//
//  main.m
//  Halloween Templates
//
//  Created by PixelBox on 30/09/15.
//  Copyright (c) 2015 PixelBox. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
