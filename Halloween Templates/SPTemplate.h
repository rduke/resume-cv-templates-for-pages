//
//  SPTemplate.h
//  Iwork Templates
//
//  Created by PixelBox on 02/09/15.
//  Copyright (c) 2015 PixelBox. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Quartz/Quartz.h>
#import <Cocoa/Cocoa.h>


//must conform to informal IKIImageBrowserItem protocol https://developer.apple.com/library/mac/documentation/GraphicsImaging/Reference/IKImageBrowserItem_Protocol/





@interface SPTemplate : NSObject

@property (nonatomic, retain) NSString *path;
@property (nonatomic, retain) NSString *pathToTemplate;

+(SPTemplate *)emptyTemplate;



@end
