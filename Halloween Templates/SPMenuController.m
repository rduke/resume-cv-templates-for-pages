//
//  SPMenuController.m
//  Resume & CV Templates for Pages
//
//  Created by  Farid Dahiri on 1/29/16.
//  Copyright © 2016 PixelBox. All rights reserved.
//

#import "SPMenuController.h"
#import <AppKit/NSMenu.h>
#import <Cocoa/Cocoa.h>
#import <CoreServices/CoreServices.h>
#import "SPHelpWindowController.h"
#import "AppDelegate.h"


@interface SPMenuController () <NSMenuDelegate>

@property (nonatomic, strong)   SPHelpWindowController* helpWindow;

@end

@implementation SPMenuController

- (IBAction)onHelp:(id)sender
{
    self.helpWindow = [[SPHelpWindowController alloc] initWithWindowNibName:@"SPHelpWindowController"];
    [self.helpWindow showWindow:((AppDelegate*)[NSApp delegate]).window];
}


- (IBAction)onContactByMail:(NSMenuItem *)sender
{
    NSSharingService* mailShare = [NSSharingService sharingServiceNamed:NSSharingServiceNameComposeEmail];
    mailShare.recipients = @[@"support@pixelboxapps.com"];
    mailShare.subject = @"PixelBox Support - Resume & CV Templates for Pages";
    NSArray* shareItems = @[];
    [mailShare performWithItems:shareItems];
}

- (void)menuWillOpen:(NSMenu *)menu
{
    NSLog(@"%@", menu.title);
}

@end
